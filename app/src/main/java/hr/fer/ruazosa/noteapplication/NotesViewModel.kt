package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.v7.widget.DialogTitle
import java.util.*

class NotesViewModel: ViewModel() {
    var notesList = MutableLiveData<List<Note>>()

    fun getNotesRepository() {
        notesList.value = NotesRepository.notesList
    }

    fun saveNoteToRepository(note: Note) {
        NotesRepository.notesList.add(note)
    }

    fun deleteNoteAt(pos: Int) {
        NotesRepository.notesList.removeAt(pos);
        getNotesRepository();
    }

    fun updateNoteAt(pos: Int, title: String, description: String) {
        NotesRepository.notesList[pos].noteTitle = title
        NotesRepository.notesList[pos].noteDescription = description
        NotesRepository.notesList[pos].noteDate = Date()
    }
}